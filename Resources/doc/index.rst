Getting Started With DolmITSettingsBundle
=========================================


Prerequisites
-------------

This version of the bundle requires Symfony 4+.


Installation
------------

1. Download dolmitos/symfony-settings-bundle using composer
2. Enable the Bundle

Step 1: Download DolmITSettingsBundle using composer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Require the bundle with composer:

.. code-block:: bash

    $ composer require dolmitos/symfony-settings-bundle

Composer will install the bundle to your project's ``vendor/dolmitos/symfony-settings-bundle`` directory.

Step 2: Enable the bundle
~~~~~~~~~~~~~~~~~~~~~~~~~

Enable the bundle in the kernel::

    <?php
    // app/AppKernel.php

    public function registerBundles()
    {
        $bundles = array(
            // ...
            new DolmIT\SettingsBundle\DolmITSettingsBundle(),
            // ...
        );
    }
    ?>


Step 3: Enable cache (Optional)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cache Adapters:
DolmIT\SettingsBundle\CacheAdapter\SymfonyTagAwareCacheComponentAdapter - Allows tags to be used, faster cache invalidation
DolmIT\SettingsBundle\CacheAdapter\SymfonyCacheComponentAdapter
DolmIT\SettingsBundle\CacheAdapter\DoctrineCacheBundleAdapter
DolmIT\SettingsBundle\CacheAdapter\NullAdapter - default (No cache)
Add parameter with the desired CacheAdapter::

    parameters:
        dolmitos_settings_cache_adapter_class: DolmIT\SettingsBundle\CacheAdapter\SymfonyCacheComponentAdapter

Add service for the cache provider::


    services:
        dolmitos_settings_cache_provider:
            class: Symfony\Component\Cache\Adapter\RedisAdapter
            public: false
            arguments:
                - '@cache.default_redis_provider'
                - 'dolmitos_settings'
                - '127001'


TagAware cache provider::


    dolmitos_settings_cache_provider_adapter:
        class: Symfony\Component\Cache\Adapter\RedisAdapter
        public: false
        arguments:
            - '@cache.default_redis_provider'
            - 'dolmitos_settings'
            - '127001'

    dolmitos_settings_cache_provider:
        class: Symfony\Component\Cache\Adapter\TagAwareAdapter
        arguments: [ '@dolmitos_settings_cache_provider_adapter' ]
