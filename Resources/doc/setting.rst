Configuring Settings
====================


General info
--------------

1. Problem areas:
1.1. Dynamic settings should not reference one-another in a circular reference.
     This is because when a setting references another, the other setting will be reloaded
     through the SettingsManager, should that new setting reference the first in any circular manner,
     the cycle will never break.
     There is no clear way to fix this limitation currently, as this process cannot be made asynchronious without
     sacrificing immutability of the setting

Static Setting
--------------

1. Structure of the setting::


    class ContactTypeSetting extends StaticSetting
    {
        const PERSON = 1;
        const COMPANY = 2;

        public function getDefault() {
            return static::PERSON;
        }

    }

1.1. extends StaticSetting
1.2. getDefault() should return the default value for the setting
1.3. All settings are configured as constants and it is assumed the values are integers, but strings would work, in theory.

2. Translations::


    <?xml version="1.0"?>
    <xliff version="1.2" xmlns="urn:oasis:names:tc:xliff:document:1.2">
        <file source-language="en" datatype="plaintext" original="file.ext">
            <body>
                <!-- ContactTypeSetting -->
                <trans-unit id="setting_contact_type">
                    <source>contact_type</source>
                    <target>Contact Type</target>
                </trans-unit>
                <trans-unit id="setting_contact_type_person">
                    <source>setting_contact_type_person</source>
                    <target>Person</target>
                </trans-unit>
                <trans-unit id="setting_contact_type_company">
                    <source>setting_contact_type_company</source>
                    <target>Company</target>
                </trans-unit>
            </body>
        </file>
    </xliff>

2.1. Setting id logic
     'setting' . '_' . 'class_name(Removes Setting from name,lowercase)' . '_' . 'constant_name(lowercase)'
2.2. To change translation domain, add this to the setting::


    private $translationDomain = 'settings';




Dynamic Setting
---------------

1. Structure of Setting::

    class CountrySetting extends DynamicSetting
    {

        const ENTITY_CLASS = SettingCountry::class;

    }

2. Structure of Entity::

    class SettingCountry implements SettingEntityInterface
    {
        // Populate Entity as you would normally
    }


3. Translations - Currently not configured, but in the future most likely tied to the Entity through a translatable bundle.

4. Collections::



    protected static $collectionVariables = [
        'name'
    ];

4.1. Overwrite $collectionVariables with the vars you want to be included in the Collection for that Setting.
4.2. Collections consist of a list of the settings with their keys and a SettingCollectionItem object with the data set in $collectionVariables
4.3. It is recommended to keep Dynamic Settings relatively small, if you wish to rely on Collections and collectionVariables heavily, though cache will handle most of the heavy lifting.