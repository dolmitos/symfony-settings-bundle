<?php

namespace DolmIT\SettingsBundle\CacheAdapter;

use Psr\Cache\CacheItemPoolInterface;
use Symfony\Component\Cache\Adapter\TagAwareAdapter;
use Symfony\Component\Cache\Adapter\TagAwareAdapterInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

class SymfonyTagAwareCacheComponentAdapter implements TagAwareCacheAdapterInterface
{

	/**
	 * @var TagAwareAdapter
	 */
	private $cache;

	public function __construct(TagAwareAdapterInterface $cache) {
		$this->cache = $cache;
	}

	public function clear() {
		return $this->cache->clear();
	}

	public function has($key) {
		return $this->cache->hasItem($key);
	}

	public function get($key) {
		return $this->cache->getItem($key)->get();
	}

	public function delete($key)
    {
        return $this->cache->deleteItem($key);
    }

    public function set($key, $value) {
		$cacheItem = $this->cache->getItem($key);
		$cacheItem->set($value);
		$cacheItem->tag($this->getTagKey($key));

		return $this->cache->save($cacheItem);
	}

	public function setMultiple(array $keysAndValues) {
		foreach ($keysAndValues as $key => $value) {
			$cacheItem = $this->cache->getItem($key);
			$cacheItem->set($value);
			$cacheItem->tag($this->getTagKey($key));
			if (!$this->cache->saveDeferred($cacheItem)) {
				return false;
			}
		}

		return $this->cache->commit();
	}

	public function invalidateTags(array $tags)
    {
        return $this->cache->invalidateTags($tags);
    }

    public function getTagKey($settingKey)
    {
        if (strpos($settingKey, '|') !== false) {
            return explode('|', $settingKey)[0];
        } else return $settingKey;
    }

}
