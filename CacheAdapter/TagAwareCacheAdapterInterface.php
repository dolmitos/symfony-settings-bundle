<?php

namespace DolmIT\SettingsBundle\CacheAdapter;

interface TagAwareCacheAdapterInterface extends CacheAdapterInterface {

    /**
     * Invalidates cached items using tags.
     *
     * @param string[] $tags An array of tags to invalidate
     *
     * @return bool True on success
     *
     * @throws InvalidArgumentException When $tags is not valid
     */
    public function invalidateTags(array $tags);

}
