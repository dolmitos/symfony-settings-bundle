<?php

namespace DolmIT\SettingsBundle\CacheAdapter;

use Doctrine\Common\Cache\CacheProvider;

class DoctrineCacheBundleAdapter implements CacheAdapterInterface {

	/**
	 * @var CacheProvider
	 */
	private $cache;

	public function __construct(CacheProvider $cache) {
		$this->cache = $cache;
	}

	public function clear() {
		return $this->cache->deleteAll();
	}

	public function has($key) {
		return $this->cache->contains($key);
	}

	public function get($key) {
		return $this->cache->fetch($key);
	}

	public function delete($key) {
		return $this->cache->delete($key);
	}

	public function set($key, $value) {
		return $this->cache->save($key, $value);
	}

	public function setMultiple(array $keysAndValues) {
		return $this->cache->saveMultiple($keysAndValues);
	}

}
