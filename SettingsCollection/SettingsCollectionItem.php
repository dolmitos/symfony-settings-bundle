<?php

namespace DolmIT\SettingsBundle\SettingsCollection;

/**
 * Class SettingsCollectionItem
 * @package DolmIT\SettingsBundle\SettingsCollection
 */
class SettingsCollectionItem
{
    /**
     * @var array
     */
    private $data;

    /**
     * SettingsCollectionItem constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = []) {
        $this->setData($data);
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     *
     * @return SettingsCollectionItem
     */
    public function setData(array $data): SettingsCollectionItem
    {
        $this->data = $data;
        return $this;
    }


    /**
     * @param $name
     *
     * @return mixed|null
     */
    public function get($name)
    {
        return $this->data[$name] ?? null;
    }

    /**
     * @param $name
     * @param $value
     *
     * @return $this
     */
    public function set($name, $value)
    {
        $this->data[$name] = $value;
        return $this;
    }

    /**
     * @param $method
     * @param $arguments
     *
     * @return mixed|null
     */
    public function __call($method, $arguments)
    {
        $command = substr($method, 0, 3);
        $field = lcfirst(substr($method, 3));
        if ($command == "set") {
            $this->set($field, $arguments[0]);
        } else if ($command == "get") {
            return $this->get($field);
        } else {
            throw new \BadMethodCallException("There is no method " . $method . " in " . __CLASS__);
        }
    }

}
