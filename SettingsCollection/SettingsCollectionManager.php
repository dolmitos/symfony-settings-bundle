<?php

namespace DolmIT\SettingsBundle\SettingsCollection;

use DolmIT\SettingsBundle\Exception\SettingEntityFieldAccessorNotFoundException;
use DolmIT\SettingsBundle\Settings\SettingEntityBridge;
use DolmIT\SettingsBundle\CacheAdapter\CacheAdapterInterface;
use App\Repository\Settings\SettingCountryRepository;
use DolmIT\SettingsBundle\Settings\DynamicSetting;
use DolmIT\SettingsBundle\Settings\SettingEntityInterface;
use DolmIT\SettingsBundle\Settings\SettingsManagerInterface;
use DolmIT\SettingsBundle\Settings\StaticSetting;
use http\Exception\BadMethodCallException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Tightenco\Collect\Support\Collection;

/**
 * Class SettingsCollectionManager
 * @package DolmIT\SettingsBundle\SettingsCollection
 */
class SettingsCollectionManager
{

    /**
     * @var NormalizerInterface
     */
    private $normalizer;


    /**
     * @var SettingEntityBridge
     */
    private $settingEntityBridge;

    /**
     * @var CacheAdapterInterface
     */
    private $cacheAdapter;

    /**
     * @var Collection[]
     */
    protected $settingsCollections = [];


    /**
     * @var SettingsManagerInterface
     */
    protected $settingsManager;

    /**
     * @return SettingsManagerInterface
     */
    public function getSettingsManager(): SettingsManagerInterface
    {
        return $this->settingsManager;
    }

    /**
     * @param SettingsManagerInterface $settingsManager
     *
     * @return SettingsCollectionManager
     */
    public function setSettingsManager(SettingsManagerInterface $settingsManager): SettingsCollectionManager
    {
        $this->settingsManager = $settingsManager;
        return $this;
    }

    /**
     * @return NormalizerInterface
     */
    public function getNormalizer(): NormalizerInterface
    {
        return $this->normalizer;
    }

    /**
     * @param NormalizerInterface $normalizer
     *
     * @return SettingsCollectionManager
     */
    public function setNormalizer(NormalizerInterface $normalizer): SettingsCollectionManager
    {
        $this->normalizer = $normalizer;
        return $this;
    }

    /**
     * @return SettingEntityBridge
     */
    public function getSettingEntityBridge(): SettingEntityBridge
    {
        return $this->settingEntityBridge;
    }

    /**
     * @param SettingEntityBridge $settingEntityBridge
     *
     * @return SettingsCollectionManager
     */
    public function setSettingEntityBridge(SettingEntityBridge $settingEntityBridge): SettingsCollectionManager
    {
        $this->settingEntityBridge = $settingEntityBridge;
        return $this;
    }


    /**
     * @param CacheAdapterInterface $cache
     *
     * @return SettingsManagerInterface
     */
    public function setCacheAdapter(CacheAdapterInterface $cacheAdapter): SettingsCollectionManager
    {
        $this->cacheAdapter = $cacheAdapter;
        return $this;
    }

    /**
     * @return CacheAdapterInterface
     */
    public function getCacheAdapter(): CacheAdapterInterface
    {
        return $this->cacheAdapter;
    }

    /**
     * @param $collectionKey
     *
     * @return bool
     */
    public function hasCollectionMemory($collectionKey)
    {
        return
            isset($this->settingsCollections[$collectionKey]) &&
            $this->settingsCollections[$collectionKey] instanceof Collection &&
            $this->settingsCollections[$collectionKey]->count() > 0
            ;
    }

    /**
     * @param $collectionKey
     *
     * @return Collection
     */
    public function getCollectionMemory($collectionKey): Collection
    {
        return $this->settingsCollections[$collectionKey] ?? collect();
    }

    /**
     * @param            $collectionKey
     * @param Collection $collection
     *
     * @return $this
     */
    public function setCollectionMemory($collectionKey, Collection $collection)
    {
        $this->settingsCollections[$collectionKey] = $collection;
        return $this;
    }

    /**
     * @param $collectionKey
     */
    public function removeCollectionMemory($collectionKey)
    {
        unset($this->settingsCollections[$collectionKey]);
    }

    /**
     * @param $collectionKey
     *
     * @return bool
     */
    public function hasCollectionCache($collectionKey)
    {
        return $this->getCacheAdapter()->has($collectionKey);
    }

    /**
     * @param $collectionKey
     *
     * @return Collection
     */
    public function getCollectionCache($collectionKey): Collection
    {
        return $this->getCacheAdapter()->get($collectionKey) ?? collect();
    }

    /**
     * @param            $collectionKey
     * @param Collection $collection
     */
    public function setCollectionCache($collectionKey, Collection $collection)
    {
        $this->getCacheAdapter()->set($collectionKey, $collection);
    }

    /**
     * @param $collectionKey
     */
    public function removeCollectionCache($collectionKey)
    {
        $cacheAdapter = $this->getCacheAdapter();
        if ($cacheAdapter instanceof TagAwareCacheAdapterInterface) {
            $cacheAdapter->invalidateTags([$collectionKey]);
        } else {
            $this->getCacheAdapter()->delete($collectionKey);
        }
    }

    /**
     * @param $collectionKey
     *
     * @return bool
     * @throws \DolmIT\SettingsBundle\Exception\SettingEntityNotFoundException
     */
    public function hasCollectionDatabase($collectionKey)
    {
        $settingClass = $this->getClassFromCollectionKey($collectionKey);
        $settingRepository = $this->getSettingEntityBridge()->getSettingRepository($settingClass);

        return $settingRepository->count([]) > 0;
    }

    /**
     * @param $collectionKey
     *
     * @return \Illuminate\Support\Collection|Collection
     * @throws SettingEntityFieldAccessorNotFoundException
     * @throws \DolmIT\SettingsBundle\Exception\SettingEntityNotFoundException
     */
    public function getCollectionDatabase($collectionKey): Collection
    {
        $settingClass = $this->getClassFromCollectionKey($collectionKey);

        $settingRepository = $this->getSettingEntityBridge()->getSettingRepository($settingClass);

        $collectionVariables = $settingClass::getCollectionVariables();

        /* @var $settingEntities SettingEntityInterface[] */
        $settingEntities = $settingRepository->findAll();

        $settingsCollection = collect();
        
        if ($settingEntities) {
            // ID is mandatory
            array_unshift($collectionVariables,'id');
            $variableAccessors = $this->getSettingEntityBridge()->getEntityAccessors($settingEntities[0], $collectionVariables);

            foreach ($settingEntities as $setting) {
                $collectionVariables = array_unique($collectionVariables);
                $data = [];

                foreach ($variableAccessors as $variable => $accessor) {
                    $value = $setting->{$accessor}();
                    if (is_int($value) || is_string($value) || is_bool($value)) $data[$variable] = $value;
                    else $data[$variable] = $this->getNormalizer()->normalize($value);
                }

                if ($data) {
                    $settingsCollection->push(new SettingsCollectionItem($data));
                }
            }


        }

        return $settingsCollection;
    }

    /**
     * @param            $collectionKey
     * @param Collection $collection
     */
    public function setCollectionDatabase($collectionKey, Collection $collection)
    {
        throw new BadMethodCallException('Method setCollectionDatabase cannot be used');
    }

    /**
     * @param string $settingClass
     *
     * @return bool
     */
    public function hasCollection(string $settingClass)
    {
        $collectionKey = $this->getCollectionKey($settingClass);

        return true;
    }

    /**
     * @param string $settingClass
     * @param bool   $refreshCache
     *
     * @return null|Collection
     * @throws SettingEntityFieldAccessorNotFoundException
     * @throws \DolmIT\SettingsBundle\Exception\SettingEntityNotFoundException
     */
    public function getCollection(string $settingClass, $refreshCache = false): ?Collection
    {
        $collectionKey = $this->getCollectionKey($settingClass);
        $collection = null;

        if ($refreshCache) {
            $this->removeCollectionMemory($collectionKey);
            $this->removeCollectionCache($collectionKey);
        }

        if (!$refreshCache && $this->hasCollectionMemory($collectionKey)) {
            return $this->getCollectionMemory($collectionKey);
        }

        if (!$refreshCache && $this->hasCollectionCache($collectionKey)) {
            $collection = $this->getCollectionCache($collectionKey);

            $this->setCollectionMemory($collectionKey, $collection);
            return $collection;
        }


        if (get_parent_class($settingClass) == StaticSetting::class) {
            /* @var $dummyClass StaticSetting*/
            $dummyClass = new $settingClass();
            $collection = collect();
            foreach ($dummyClass->getList() as $identifier => $key) {
                $collection->put($key,new SettingsCollectionItem(['id' => $key, 'identifier' => $identifier]));
            }
        } elseif (get_parent_class($settingClass) == DynamicSetting::class) {
            if ($this->hasCollectionDatabase($collectionKey)) {
                $collection = $this->getCollectionDatabase($collectionKey);
            }
        } else {
            $collection = collect();
        }

        if ($collection && $collection->count()) {
            $this->setCollectionMemory($collectionKey, $collection);
            $this->setCollectionCache($collectionKey, $collection);
        }


        return $collection;
    }


    /**
     * @param string     $settingClass
     * @param Collection $collection
     *
     * @return SettingsCollectionManager
     */
    public function setCollection(string $settingClass, Collection $collection): SettingsCollectionManager
    {
        $collectionKey = $this->getCollectionKey($settingClass);

        $this->setCollectionMemory($collectionKey, $collection);

        $this->setCollectionCache($collectionKey, $collection);

        return $this;
    }

    public function removeCollection(string $settingClass)
    {
        $this->removeCollectionMemory($this->getCollectionKey($settingClass));
        $this->removeCollectionCache($this->getCollectionKey($settingClass));
    }

    /**
     * Create a key for the given setting class
     *
     * @param $class
     *
     * @return string
     */
    public function getCollectionKey($class)
    {
        return str_replace('\\', '-', $class);
    }
    
    /**
     * Get class from the key
     *
     * @param $collectionKey
     *
     * @return mixed
     */
    public function getClassFromCollectionKey($collectionKey)
    {
        return str_replace('-', '\\', $collectionKey);
    }

}
