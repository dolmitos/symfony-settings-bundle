<?php

namespace DolmIT\SettingsBundle\DataTypes;

use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;
use DolmIT\SettingsBundle\Settings\GlobalSettingsManager;
use DolmIT\SettingsBundle\Settings\SettingsManagerInterface;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use DolmIT\SettingsBundle\Settings\SettingsInterface;

abstract class BaseType extends Type
{
    const SETTING_TYPE = 'setting_base';

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getIntegerTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return ContactInfoTypeSetting|mixed
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null || $value instanceof SettingsInterface) {
            return $value;
        }
        $val = null;

        try {
            $val = $this->getSettingsManager()->loadSetting($this->getSettingClass(), $value);
        } catch (\Exception $e){}

        if ( ! $val) {
            throw ConversionException::conversionFailed($value, $this->getName());
        }

        return $val;
    }

    /**
     * @param mixed            $value
     * @param AbstractPlatform $platform
     *
     * @return mixed
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (null === $value) {
            return $value;
        }

        if (is_int($value)) {
            return $value;
        }

        if (is_string($value)) {
            return $value;
        }

        if ($value instanceof SettingsInterface) {
            return $value->getValue();
        }

        throw ConversionException::conversionFailedInvalidType($value, $this->getName(), ['null','integer','varchar','binary']);
    }

    public function getName()
    {
        $className = get_class($this);
        return $className::SETTING_TYPE;
    }


    /**
     * Function to get the class of the setting responsible for this Type
     *
     * @return string
     */
    abstract public function getSettingClass():string;

    /**
     * @return SettingsManagerInterface
     */
    protected function getSettingsManager(): SettingsManagerInterface
    {
        return GlobalSettingsManager::getSettingsManager();
    }

}
