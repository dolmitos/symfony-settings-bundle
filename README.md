SettingsBundle 
====================

Dynamic and Static Settings for Symfony 4

Dolm IT
------

<img src="https://www.dolmit.com/images/logo.svg" height="40px" alt="Dolm IT logo">
<br>

[Dolm IT](https://www.dolmit.com)

Documentation
-------------

Documentation is available on [Docs](Resources/doc/index.rst).

MIT License
-----------

License can be found [here](LICENSE).

Inspiration
----------
 - [Christian Raue - CraueConfigBundle](https://github.com/craue/CraueConfigBundle)
