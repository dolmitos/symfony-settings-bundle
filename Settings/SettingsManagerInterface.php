<?php

namespace DolmIT\SettingsBundle\Settings;
use Doctrine\Common\Persistence\ObjectManager;
use DolmIT\SettingsBundle\CacheAdapter\CacheAdapterInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Tightenco\Collect\Support\Collection;

/**
 * Interface SettingsInterface
 * @package App\Settings
 */
interface SettingsManagerInterface
{

    /**
     * @param CacheAdapterInterface $cacheAdapter
     *
     * @return SettingsManagerInterface
     */
    public function setCacheAdapter(CacheAdapterInterface $cacheAdapter): SettingsManagerInterface;


    /**
     * @param ObjectManager $objectManager
     *
     * @return SettingsManagerInterface
     */
    public function setObjectManager(ObjectManager $objectManager): SettingsManagerInterface;


    /**
     * @param TranslatorInterface $translator
     *
     * @return SettingsManagerInterface
     */
    public function setTranslator(TranslatorInterface $translator): SettingsManagerInterface;



    /**
     * Function responsible for loading the appropriate settings class
     * into memory and returning the reference
     *
     * @param string $class
     * @param mixed $value
     * @return SettingsInterface
     */
    public function loadSetting(string $class, $value): SettingsInterface;

    /**
     * Save the current setting
     * Should handle cache as well
     *
     * @param DynamicSetting $setting
     *
     * @return mixed
     */
    public function saveSetting(DynamicSetting $setting);

    /**
     * Remove setting from cache and memory
     *
     * @param string $class
     * @param        $value
     */
    public function removeSetting(string $class, $value);

    /**
     * Load a collection of settings, contents are based
     * on the variables set in the class
     *
     * @param string $className
     * @param bool   $refreshCache
     *
     * @return Collection
     */
    public function loadCollection(string $className, $refreshCache = false): ?Collection;

    /**
     * Save the collection to cache
     *
     * @param string     $className
     * @param Collection $collection
     *
     * @return mixed
     */
    public function saveCollection(string $className, Collection $collection);

    /**
     * Load Collection with SettingsInterface elements
     *
     * NB! This function comes at a huge time cost.
     *
     * @param string $className
     * @param bool   $refreshCache
     *
     * @return Collection
     */
    public function loadSettingCollection(string $className, $refreshCache = false): ?Collection;

    /**
     * Remove collection from cache
     * Also removes memory
     * Functions based on tags, so only works when TagAwareAdapter is used
     *
     * @param string $className
     *
     * @return mixed
     */
    public function removeCollection(string $className);

}