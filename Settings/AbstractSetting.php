<?php

namespace DolmIT\SettingsBundle\Settings;

use ReflectionClass;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class AbstractSettings
 * @package App\Settings
 */
abstract class AbstractSetting implements SettingsInterface
{
    /**
     * @var string|integer
     */
    private $value;

    /**
     * @var string
     */
    private $translationDomain = 'settings';


    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @return string
     */
    protected function getSettingIdentifierPrefix() {
        return 'setting';
    }

    /**
     * @return string
     */
    protected function getSettingIdentifierJoinerSymbol() {
        return '_';
    }

    abstract function getDefault();

    /**
     * @return string|integer
     */
    public function getValue()
    {
        if (!$this->value) return $this->getDefault();
        return $this->value;
    }


    /**
     * @param $value
     *
     * @return self
     */
    public function setValue($value = null): self
    {
        if (!$value) $value = $this->getDefault();
        $this->value = $value;
        return $this;
    }

    /**
     * @return ReflectionClass|null
     */
    protected function getReflection() {
        try {
            return new ReflectionClass($this);
        } catch (\ReflectionException $exception) {}
        return null;
    }

    /**
     * @return string
     */
    public function getSettingIdentifier(): string {

        return $this->getSettingIdentifierPrefix().$this->getSettingIdentifierJoinerSymbol().strtolower(
            preg_replace(
                [
                    '/([a-z\d])([A-Z])/',
                    '/([^_])([A-Z][a-z])/'
                ],
                '$1'.$this->getSettingIdentifierJoinerSymbol().'$2',
                str_replace('Setting','',$this->getReflection()->getShortName())
            )
        );
    }

    /**
     * Returns true if the Setting values are statically assigned to the corresponding class
     *
     * @return bool
     */
    public function isStatic(): bool
    {
        return $this->getType() === static::SETTINGS_TYPE_STATIC;
    }

    /**
     * Returns true is the Setting values are handled by the database
     *
     * @return bool
     */
    public function isDynamic(): bool
    {
        return $this->getType() === static::SETTINGS_TYPE_DYNAMIC;
    }

    /**
     * @return array
     */
    public function getList():array
    {
        return array_diff_key($this->getReflection()->getConstants(), $this->excludeAbstractConstants());
    }

    /**
     * @return array
     */
    protected function excludeAbstractConstants(): array
    {
        try {
            $class = new ReflectionClass(self::class);
            return $class->getConstants();
        } catch (\ReflectionException $exception) {}
        return [];
    }


    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getSettingTitle(): string
    {
        if ($this->hasTranslator()) {
            return $this->getTranslator()->trans($this->getSettingIdentifier(),[],$this->getTranslationDomain());
        }
        return $this->getSettingIdentifier();
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getName(): string
    {
        return '';
    }
    
    /**
     * Find out if the translator exists
     *
     * @return bool
     */
    public function hasTranslator(): bool
    {
        return $this->translator instanceof TranslatorInterface;
    }

    /**
     * Get the translator instance
     *
     * @return null|TranslatorInterface
     */
    public function getTranslator(): ?TranslatorInterface
    {
        return $this->translator;
    }

    /**
     *  Set the translator instance
     *
     * @param null|TranslatorInterface $translator
     *
     * @return AbstractSetting
     */
    public function setTranslator(?TranslatorInterface $translator): AbstractSetting
    {
        $this->translator = $translator;
        return $this;
    }


    /**
     * {@inheritdoc}
     */
    public function setTranslationDomain($translationDomain = 'settings'): self
    {
        $this->translationDomain = $translationDomain;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getTranslationDomain(): string
    {
        return $this->translationDomain;
    }


    /**
     * {@inheritdoc}
     */
    public function __toString(): string
    {
        return $this->getName();
    }

    /**
     *
     * When receiving the setting from cache, should it contain any other settings,
     * those will be in a deserialized form and not connected to the settings system.
     * Thus we must check for any settings and reload them from the SettingsManager
     */
    private function reloadSettingProperties(SettingsManagerInterface $settingsManager) 
    {
        if (!$this->isDynamic()) throw new \Exception('Incorrect Setting type sent to reloading function', 500);
        if ($this instanceof DynamicSetting) {
            $data = $this->getData();
            foreach ($data as $key => $datum) {
                if ($datum instanceof SettingsInterface) {
                    $data[$key] = $settingsManager->loadSetting(get_class($datum), $datum->getValue());
                }
            }
            $this->setData($data);    
        }
    }


    /**
     * This function is called before Setting is saved to memory
     */
    public function preSetMemory(SettingsManagerInterface $settingsManager) {}

    /**
     * This function is called after getting the setting from memory
     */
    public function postGetMemory(SettingsManagerInterface $settingsManager) {}

    /**
     * This function is called before Setting is saved to cache.
     * This is a good time to remove items from
     * the setting that you might not want to be saved in the cache.
     * 
     * NB! preSetCache will be called on a clone of the actual element,
     * because we don't want to modify the element in memory, only the one placed
     * into cache.
     */
    public function preSetCache(SettingsManagerInterface $settingsManager) {
        $this->setTranslator(null);
    }

    /**
     * This function is called after getting the setting from cache
     */
    public function postGetCache(SettingsManagerInterface $settingsManager) {
        $this->setTranslator($settingsManager->getTranslator());
        if ($this->isDynamic()) {
            $this->reloadSettingProperties($settingsManager);   
        }
    }

    /**
     * This function is called before Setting is saved to Database.
     */
    public function preSetDatabase(SettingsManagerInterface $settingsManager) {}


    /**
     * This function is called after getting the setting from database
     */
    public function postGetDatabase(SettingsManagerInterface $settingsManager) {
        if ($this->isDynamic()) {
            $this->reloadSettingProperties($settingsManager);
        }
    }

    /**
     * This function is called after the setting has been created
     *
     * @param SettingsManagerInterface $settingsManager
     */
    public function postCreate(SettingsManagerInterface $settingsManager) {
        $this->setTranslator($settingsManager->getTranslator());
    }
}
