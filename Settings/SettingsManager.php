<?php

namespace DolmIT\SettingsBundle\Settings;

use Doctrine\Common\Persistence\ObjectManager;
use DolmIT\SettingsBundle\CacheAdapter\CacheAdapterInterface;
use DolmIT\SettingsBundle\CacheAdapter\TagAwareCacheAdapterInterface;
use DolmIT\SettingsBundle\Exception\SettingClassNotFoundException;
use DolmIT\SettingsBundle\Exception\SettingEntityNotFoundException;
use DolmIT\SettingsBundle\Exception\SettingNotFoundException;
use DolmIT\SettingsBundle\Exception\SettingsMemoryOverwriteException;
use DolmIT\SettingsBundle\SettingsCollection\SettingsCollectionManager;
use DolmIT\SettingsBundle\SettingsList\SettingsListManager;
use Symfony\Component\Translation\TranslatorInterface;
use Tightenco\Collect\Support\Collection;

/**
 * Class SettingsManager
 * @package DolmIT\SettingsBundle\Settings
 */
class SettingsManager implements SettingsManagerInterface
{

    /**
     * @var SettingEntityBridge
     */
    private $settingEntityBridge;


    /**
     * @var SettingsCollectionManager
     */
    private $settingsCollectionManager;

    /**
     * @var CacheAdapterInterface
     */
    private $cacheAdapter;

    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * Array of all registered settings on the current request
     *
     * @var array
     */
    private $settings;


    /**
     * @return SettingEntityBridge
     */
    public function getSettingEntityBridge(): SettingEntityBridge
    {
        return $this->settingEntityBridge;
    }

    /**
     * @param SettingEntityBridge $settingEntityBridge
     *
     * @return SettingsManager
     */
    public function setSettingEntityBridge(SettingEntityBridge $settingEntityBridge): SettingsManager
    {
        $this->settingEntityBridge = $settingEntityBridge;
        return $this;
    }

    /**
     * @param CacheAdapterInterface $cache
     *
     * @return SettingsManagerInterface
     */
    public function setCacheAdapter(CacheAdapterInterface $cacheAdapter): SettingsManagerInterface
    {
        $this->cacheAdapter = $cacheAdapter;
        return $this;
    }

    /**
     * @return CacheAdapterInterface
     */
    public function getCacheAdapter(): CacheAdapterInterface
    {
        return $this->cacheAdapter;
    }


    /**
     * @param TranslatorInterface $cache
     *
     * @return SettingsManagerInterface
     */
    public function setTranslator(TranslatorInterface $translator): SettingsManagerInterface
    {
        $this->translator = $translator;
        return $this;
    }

    /**
     * @return TranslatorInterface
     */
    public function getTranslator(): TranslatorInterface
    {
        return $this->translator;
    }

    /**
     * @param ObjectManager $objectManager
     *
     * @return SettingsManagerInterface
     */
    public function setObjectManager(ObjectManager $objectManager): SettingsManagerInterface
    {
        $this->objectManager = $objectManager;
        return $this;
    }

    /**
     * @return ObjectManager
     */
    public function getObjectManager(): ObjectManager
    {
        return $this->objectManager;
    }

    /**
     * @param ObjectManager $objectManager
     *
     * @return SettingsManagerInterface
     */
    public function setSettingsCollectionManager(SettingsCollectionManager $settingsCollectionManager): SettingsManagerInterface
    {
        $this->settingsCollectionManager = $settingsCollectionManager;
        return $this;
    }

    /**
     * @return ObjectManager
     */
    public function getSettingsCollectionManager(): SettingsCollectionManager
    {
        return $this->settingsCollectionManager;
    }

    /**
     * Check if setting is available in running memory (fastest)
     *
     * @param $settingKey
     *
     * @return bool
     */
    public function hasSettingMemory($settingKey)
    {
        return isset($this->settings[$settingKey]);
    }


    /**
     * Get the setting object from running memory
     * Unless memory has been turned off, every setting
     * holder is referencing an object in SettingsManager memory.
     *
     * @param $settingKey
     *
     * @return mixed
     */
    public function getSettingMemory($settingKey)
    {
        /* @var $setting AbstractSetting */
        $setting = $this->settings[$settingKey];
        $setting->postGetMemory($this);
        return $setting;
    }

    /**
     * Overwrite key with a settings object.
     * Existing settings cannot be overwritten.
     *
     * @param                 $settingKey
     * @param AbstractSetting $setting
     */
    public function setSettingMemory($settingKey, AbstractSetting $setting)
    {
        if (isset($this->settings[$settingKey])) {
            throw new SettingsMemoryOverwriteException(
                'Attempt to overwrite existing setting object in memory - ' .
                'The object held here is being referenced by everything that uses' .
                ' this specific setting value combination and cannot be overwritten.'
            );
        }
        $setting->preSetMemory($this);
        $this->settings[$settingKey] = $setting;
    }

    /**
     * @param $settingKey
     */
    public function removeSettingMemory($settingKey)
    {
        unset($this->settings[$settingKey]);
    }

    /**
     * Check if setting exists in cache (fast)
     *
     * @param $settingKey
     *
     * @return bool
     */
    public function hasSettingCache($settingKey)
    {
        return $this->getCacheAdapter()->has($settingKey);
    }

    /**
     *
     * @param $settingKey
     *
     * @return mixed
     */
    public function getSettingCache($settingKey)
    {
        $setting = $this->getCacheAdapter()->get($settingKey);
        $setting->postGetCache($this);
        return $setting;
    }

    /**
     * Save settings to cache
     * Remove translator to minimize cache usage
     *
     * @param                 $settingKey
     * @param AbstractSetting $setting
     */
    public function setSettingCache($settingKey, AbstractSetting $setting)
    {
        // We need to close because we don't want the event to touch the object in memory
        $cloneSetting = clone $setting;
        $cloneSetting->preSetCache($this);
        $this->getCacheAdapter()->set($settingKey, $cloneSetting);
    }

    /**
     * @param $settingKey
     */
    public function removeSettingCache($settingKey)
    {
        $this->getCacheAdapter()->delete($settingKey);
    }


    /**
     *
     *
     * @param $settingKey
     *
     * @return bool
     * @throws SettingEntityNotFoundException
     */
    public function hasSettingDatabase($settingKey)
    {
        $settingClass = $this->getClassFromKey($settingKey);
        $settingValue = $this->getValueFromKey($settingKey);

        $settingRepository = $this->getSettingEntityBridge()->getSettingRepository($settingClass);

        return $settingRepository->count(['id' => $settingValue]) > 0;
    }

    /**
     * @param $settingKey
     *
     * @return AbstractSetting
     * @throws SettingEntityNotFoundException
     * @throws \DolmIT\SettingsBundle\Exception\SettingsFactoryException
     */
    public function getSettingDatabase($settingKey)
    {
        $settingClass = $this->getClassFromKey($settingKey);
        $settingValue = $this->getValueFromKey($settingKey);

        $settingRepository = $this->getSettingEntityBridge()->getSettingRepository($settingClass);

        $settingEntity = $settingRepository->findOneBy(['id' => $settingValue]);

        $settingsFactory = new SettingsFactory();
        $setting = $settingsFactory->create($settingClass, $settingValue);
        $setting->postCreate($this);

        $this->getSettingEntityBridge()->entityToSetting($settingEntity, $setting);
        $setting->postGetDatabase($this);

        return $setting;
    }

    /**
     * @param                 $settingKey
     * @param AbstractSetting $setting
     *
     * @throws SettingEntityNotFoundException
     */
    public function setSettingDatabase($settingKey, AbstractSetting $setting)
    {
        $setting->preSetDatabase($this);

        $settingClass = $this->getClassFromKey($settingKey);
        $settingValue = $this->getValueFromKey($settingKey);

        $settingRepository = $this->getSettingEntityBridge()->getSettingRepository($settingClass);
        $settingEntity = $settingRepository->findOneBy(['id' => $settingValue]);

        $this->getSettingEntityBridge()->settingToEntity($setting, $settingEntity);

        $this->getObjectManager()->persist($settingEntity);
        $this->getObjectManager()->flush();
    }



    /**
     * @param string $settingKey Class and value combined key of the setting.
     *
     * @return string|null Value of the setting.
     * @throws \RuntimeException If the setting is not defined.
     */
    public function get($settingKey, $refreshCache = false)
    {
        $setting = null;

        if ($refreshCache) {
            $this->removeSettingMemory($settingKey);
            $this->removeSettingCache($settingKey);
        }

        // First level hit, no further action required
        if (!$refreshCache && $this->hasSettingMemory($settingKey)) {
            $setting = $this->getSettingMemory($settingKey);
            return $setting;
        }

        // Second level hit, we need to update first level
        if (!$refreshCache && $this->hasSettingCache($settingKey)) {
            $setting = $this->getSettingCache($settingKey);

            $this->setSettingMemory($settingKey, $setting);
            return $setting;
        }

        // Third level is different for static and dynamic settings
        $settingClass = $this->getClassFromKey($settingKey);
        $settingValue = $this->getValueFromKey($settingKey);

        if (get_parent_class($settingClass) == StaticSetting::class) {
            $settingValue = $this->getValueFromKey($settingKey);
            $settingsFactory = new SettingsFactory();
            $setting = $settingsFactory->create($settingClass, $settingValue);
            $setting->postCreate($this);
            $setting->load($settingValue);
        } elseif (get_parent_class($settingClass) == DynamicSetting::class) {
            if ($this->hasSettingDatabase($settingKey)) {
                $setting = $this->getSettingDatabase($settingKey);
            }
        }

        // Third level hit, we need to update first and second level
        if ($setting) {
            $this->setSettingMemory($settingKey, $setting);
            $this->setSettingCache($settingKey, $setting);
            return $setting;
        }

        // No hit, this should not happen
        throw new SettingNotFoundException('SettingsManager->get() - Class ' . $settingClass . ' with value ' . $settingValue . ' not found.');
    }

    /**
     * Only Dynamic Settings can be set
     *
     * @param string      $name  Name of the setting to update.
     * @param string|null $value New value for the setting.
     *
     * @throws \RuntimeException If the setting is not defined.
     */
    public function set($settingKey, AbstractSetting $setting)
    {
        // We should never override something in memory, as that provides a reference to all consumers utilizing the setting
        try {
            $existingSetting = $this->get($settingKey);
        } catch (SettingNotFoundException $e) {
            $existingSetting = false;
        }

        if ($existingSetting) {
            // If the setting references the same object, no override is required
            if ($existingSetting !== $setting) {
                $existingSetting->setData($setting->getData());
                unset($setting);
                $setting = $existingSetting;
            }
        } else {
            // Set to Memory only if it does not yet exist
            $this->setSettingMemory($settingKey, $setting);
        }

        if ($setting instanceof DynamicSetting) {
            $this->setSettingDatabase($settingKey, $setting);
        }

        // Save cache later, as DB save could fail, in theory
        $this->setSettingCache($settingKey, $setting);

        // Update Collection
        // TODO update only the fields that were changed, we do not need to do so much.
        $this->getSettingsCollectionManager()->getCollection(get_class($setting),true);
    }

    /**
     * @param string $className
     * @param bool   $refreshCache
     *
     * @return Collection
     */
    public function loadCollection(string $className, $refreshCache = false): ?Collection
    {
        return $this->getCollection($className, $refreshCache);
    }

    /**
     * @param string $className
     *
     * @return null|Collection
     * @throws SettingEntityNotFoundException
     * @throws \DolmIT\SettingsBundle\Exception\SettingEntityFieldAccessorNotFoundException
     */
    public function getCollection(string $className, $refreshCache = false)
    {
        if ($this->getSettingsCollectionManager()->hasCollection($className)) {
            return $this->getSettingsCollectionManager()->getCollection($className, $refreshCache);
        }
    }

    /**
     * @param string     $className
     * @param Collection $collection
     *
     * @return SettingsManager|mixed
     */
    public function saveCollection(string $className, Collection $collection)
    {
        return $this->setCollection($className, $collection);
    }

    /**
     * @param            $className
     * @param Collection $collection
     *
     * @return $this
     */
    public function setCollection(string $className, Collection $collection)
    {
        $this->getSettingsCollectionManager()->setCollection($className, $collection);
        return $this;
    }

    /**
     * @param string $className
     */
    public function removeCollection(string $className)
    {
        $this->getSettingsCollectionManager()->removeCollection($className);
    }

    /**
     * Convert Collection to Objects of SettingsInterface
     *
     * @param string     $className
     * @param Collection $collection
     *
     * @return Collection
     */
    public function convertCollection(string $className, Collection $collection, $refrashCache = false): ?Collection
    {
        if ($collection->first() instanceof SettingsInterface) return $collection;
        $settingsManager = $this;
        return $collection->map(function($item) use ($settingsManager,$className, $refrashCache) {return $settingsManager->loadSetting($className,$item->get('id'), $refrashCache);});
    }

    /**
     * Load Collection with SettingsInterface elements
     *
     * @param string $className
     * @param bool   $refreshCache
     *
     * @return Collection
     */
    public function loadSettingCollection(string $className, $refreshCache = false): ?Collection
    {
        $collection = $this->loadCollection($className, $refreshCache);
        $collection = $this->convertCollection($className, $collection, $refreshCache);
        return $collection;
    }


    /**
     * {@inheritdoc}
     *
     * @param string $class
     * @param mixed  $value
     *
     * @return SettingsInterface
     */
    public function loadSetting(string $class, $value, $refreshCache = false): SettingsInterface
    {
        if (class_exists($class)) {
            $setting = $this->get($this->getSettingKey($class, $value), $refreshCache);
        } else {
            throw new SettingClassNotFoundException('loadSetting - Class ' . $class . ' not found.');
        }

        return $setting;
    }

    /**
     * Save the changes made to a setting to all levels
     * Only allowed for DynamicSetting types
     *
     * @param DynamicSetting $setting
     */
    public function saveSetting(DynamicSetting $setting)
    {
        $settingClass = get_class($setting);
        $settingValue = $setting->getValue();
        $settingKey = $this->getSettingKey($settingClass, $settingValue);
        $this->set($settingKey, $setting);
    }

    /**
     * Remove setting from cache and memory
     *
     * @param string $class
     * @param        $value
     */
    public function removeSetting(string $class, $value)
    {
        $this->removeSettingCache($this->getSettingKey($class, $value));
        $this->removeSettingMemory($this->getSettingKey($class, $value));
    }

    /**
     * Create a key for the given setting class and value
     *
     * @param $class
     * @param $value
     *
     * @return string
     */
    public function getSettingKey(string $class, $value)
    {
        return str_replace('\\', '-', $class) . '|' . $value;
    }

    /**
     * Returns name of tag by class
     *
     * @param $class
     *
     * @return mixed
     */
    public function getTagName(string $class)
    {
        return str_replace('\\', '-', $class);
    }

    /**
     * Get class from the key
     *
     * @param $settingKey
     *
     * @return mixed
     */
    public function getClassFromKey($settingKey)
    {
        return str_replace('-', '\\', explode('|', $settingKey)[0]);
    }

    /**
     * Get value from the key
     *
     * @param $settingKey
     *
     * @return mixed
     */
    public function getValueFromKey($settingKey)
    {
        return explode('|', $settingKey)[1];
    }

}
