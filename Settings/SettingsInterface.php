<?php

namespace DolmIT\SettingsBundle\Settings;

use Symfony\Component\Translation\TranslatorInterface;

/**
 * Interface SettingsInterface
 * @package App\Settings
 */
interface SettingsInterface
{
    /**
     * Type for static Settings with predefined values
     */
    const SETTINGS_TYPE_STATIC = 1;

    /**
     * Type for dynamic Settings with database or other alterable settings
     */
    const SETTINGS_TYPE_DYNAMIC = 2;

    /**
     * @return mixed
     */
    public function getDefault();

    public function setTranslator(?TranslatorInterface $translator);

    /**
     * Returns the identifier for the current setting type.
     * Could be relied upon for translation, as it is tied to the class name
     *
     * @return string
     */
    public function getSettingIdentifier(): string;

    /**
     * @return int
     */
    public function getType(): int;

    /**
     * Returns the title for the setting or the translation
     * string if Translator instance has not been set.
     *
     * @return string
     */
    public function getSettingTitle(): string;

    /**
     * Returns the title for the setting value or the translation
     * string if Translator instance has not been set.
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Get the value identifier of the Setting
     *
     * @return string|integer
     */
    public function getValue();


    /**
     * Load the data into the setting class
     *
     * @param $value
     *
     * @return SettingsInterface
     */
    public function load($value);

}