<?php

namespace DolmIT\SettingsBundle\Settings;

use Symfony\Component\Translation\TranslatorInterface;

/**
 * Interface SettingsInterface
 * @package App\Settings
 */
interface SettingEntityInterface
{

    /**
     * Get the name of the Setting
     * This is meant to be a singular description
     * of the setting.
     * If the setting has many fields, getName should return
     * one or many of the values combined as a general name.
     *
     * The content is up to the defining class
     * 
     * This value should not be empty.
     *
     * @return string
     */
    public function getName():string;



}