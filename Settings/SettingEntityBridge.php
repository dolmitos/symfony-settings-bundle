<?php
namespace DolmIT\SettingsBundle\Settings;

use DolmIT\SettingsBundle\Exception\SettingEntityFieldAccessorNotFoundException;
use DolmIT\SettingsBundle\Exception\SettingEntityNotFoundException;
use Doctrine\Common\Persistence\ObjectManager;

class SettingEntityBridge
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @param ObjectManager $objectManager
     *
     * @return SettingsManagerInterface
     */
    public function setObjectManager(ObjectManager $objectManager): SettingEntityBridge
    {
        $this->objectManager = $objectManager;
        return $this;
    }

    /**
     * @return ObjectManager
     */
    public function getObjectManager(): ObjectManager
    {
        return $this->objectManager;
    }


    /**
     * Return Setting Entity Class if found or null if setting is static
     *
     * @param $settingClass
     *
     * @return string|null
     * @throws SettingEntityNotFoundException
     */
    public function getSettingEntityClass($settingClass): ?string
    {
        if (class_exists($settingClass)) {
            if (get_parent_class($settingClass) == StaticSetting::class) return null;

            /* @var $settingClass DynamicSetting */
            if ($settingClass::ENTITY_CLASS && class_exists($settingClass::ENTITY_CLASS)) {
                return $settingClass::ENTITY_CLASS;
            }
        }

        throw new SettingEntityNotFoundException(
            'SettingsManager->getSettingEntityClass() - ' .
            'Entity for class ' . $settingClass . ' not found, ' .
            'make sure you have added an existing class to the constant "ENTITY_CLASS"');
    }

    /**
     * @param $settingClass
     *
     * @return \Doctrine\Common\Persistence\ObjectRepository
     * @throws SettingEntityNotFoundException
     */
    public function getSettingRepository($settingClass)
    {
        $entityClass = $this->getSettingEntityClass($settingClass);
        $settingRepository = $this->getObjectManager()->getRepository($entityClass);

        return $settingRepository;
    }
    

    /**
     * Move entity values to setting class data field
     *
     * @param SettingEntityInterface $settingEntity
     * @param DynamicSetting         $setting
     */
    public function entityToSetting(SettingEntityInterface $settingEntity, DynamicSetting $setting) {
        $setting->setName($settingEntity->getName());

        $data = $setting->getData();

        $objectMethods = get_class_methods($settingEntity);
        $getters = array_filter($objectMethods, function($method) {
            return (
                (strpos($method, 'get') !== false && strpos($method, 'get') == 0 && $method !== 'getId') ||
                (strpos($method, 'is') !== false && strpos($method, 'is') == 0) ||
                (strpos($method, 'has') !== false && strpos($method, 'has') == 0)
            );
        });

        foreach ($getters as $getter) {
            if (strpos($getter, 'get') !== false && strpos($getter, 'get') == 0) $key = lcfirst(ltrim($getter,'get'));
            if (strpos($getter, 'is') !== false && strpos($getter, 'is') == 0) $key = lcfirst(ltrim($getter,'is'));
            if (strpos($getter, 'has') !== false && strpos($getter, 'has') == 0) $key = lcfirst(ltrim($getter,'has'));
            $data[$key] = $settingEntity->{$getter}();
        }

        $setting->setData($data);
    }

    /**
     * Move setting values to entity through setters
     *
     * @param DynamicSetting         $setting
     * @param SettingEntityInterface $settingEntity
     */
    public function settingToEntity(DynamicSetting $setting, SettingEntityInterface $settingEntity) {
        $data = $setting->getData();

        $objectMethods = get_class_methods($settingEntity);
        $setters = array_filter($objectMethods, function($method) {
            return (
                (strpos($method, 'set') !== false && strpos($method, 'set') == 0 && $method !== 'setId')
            );
        });

        foreach ($setters as $setter) {
            $key = lcfirst(ltrim($setter,'set'));
            $settingEntity->{$setter}($data[$key]);
        }
    }

    public function getEntityAccessors(SettingEntityInterface $settingEntity, array $variables = [])
    {
        $variableAccessors = [];
        $potentialAccessors = ['get','has','is',''];
        foreach ($variables as $variable) {
            foreach ($potentialAccessors as $potentialAccessor) {
                $entityFieldAccessor = $potentialAccessor ? $potentialAccessor.ucfirst($variable) : $variable;
                if (method_exists($settingEntity,$entityFieldAccessor)) {
                    $variableAccessors[$variable] = $entityFieldAccessor;
                    break;
                }
            }
            if (!isset($variableAccessors[$variable])) {
                throw new SettingEntityFieldAccessorNotFoundException(
                    'Could not find accessor for collection variable ' . $variable .
                    ' in Entity: '. get_class($settingEntity)
                );
            }
        }

        return $variableAccessors;
    }

}
