<?php

namespace DolmIT\SettingsBundle\Settings;

/**
 * The purpose of this Global Class is to share the
 * SettingsManager instance between all classes
 * that otherwise would not be able to reach it through DI.
 * Namely the Doctrine\DBAL\Types\Type children.
 *
 * Class GlobalSettingsManager
 * @package DolmIT\SettingsBundle\Settings
 */
class GlobalSettingsManager
{

    /**
     * @var SettingsManagerInterface
     */
    private static $_settingsManager;

    /**
     * @param SettingsManagerInterface $settingsManager
     */
    public static function setSettingsManager(SettingsManagerInterface $settingsManager)
    {
        static::$_settingsManager = $settingsManager;
    }

    public static function getSettingsManager(): SettingsManagerInterface
    {
        return static::$_settingsManager;
    }

}
