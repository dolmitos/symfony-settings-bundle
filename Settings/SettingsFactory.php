<?php
namespace DolmIT\SettingsBundle\Settings;

use DolmIT\SettingsBundle\Exception\SettingsFactoryException;
use Symfony\Component\Translation\TranslatorInterface;

class SettingsFactory
{
    /**
     * Create Setting.
     *
     * @param string      $class
     *
     * @return AbstractSetting
     * @throws SettingsFactoryException
     */
    public function create($class, $value)
    {
        if (!is_string($class)) {
            $type = gettype($class);
            throw new SettingsFactoryException("SettingsFactory::create(): String expected, $type given");
        }

        if (false === class_exists($class)) {
            throw new SettingsFactoryException("SettingsFactory::create(): $class does not exist");
        }

        if (in_array(SettingsInterface::class, class_implements($class))) {
            /* @var $object AbstractSetting */
            $object = new $class();
            $object->setValue($value);
            return $object;
        } else {
            throw new SettingsFactoryException("SettingsFactory::create(): The class $class should implement the SettingInterface.");
        }
    }

}
