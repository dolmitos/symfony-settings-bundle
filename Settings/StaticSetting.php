<?php

namespace DolmIT\SettingsBundle\Settings;

use ReflectionClass;
use Symfony\Component\Translation\TranslatorInterface;

class StaticSetting extends AbstractSetting
{
    /**
     * @param $value
     *
     * @return StaticSetting
     */
    public function load($value): self
    {
        if ($value) {
            $this->setValue($value);
        }
        
        return $this;
    }

    public function getDefault()
    {
        // TODO: Implement getDefault() method.
        // TODO return first constant of the class by default
    }

    // Obtain the textual representation of the setting through it's contstant name
    public function getTextualValue()
    {
        $list = $this->getList();
        $constants = array_flip($this->getList());
        return strtolower($constants[$this->getValue()]);
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return static::SETTINGS_TYPE_STATIC;
    }


    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getName(): string
    {
        if ($this->hasTranslator()) {
            return $this->getTranslator()->trans($this->getNameTranslationString(),[],$this->getTranslationDomain());
        }

        return $this->getNameTranslationString();
    }

    /**
     * @param $settingId
     *
     * @return bool|string
     */
    public function getNameTranslationString($settingId = null)
    {
        if (!$settingId) $settingId = $this->getValue();
        $constants = array_flip($this->getList());
        if (
            $constants &&
            isset($constants[$settingId]) &&
            $constants[$settingId]
        ) {
            return $this->getSettingIdentifier().$this->getSettingIdentifierJoinerSymbol().strtolower($constants[$settingId]);
        }
        return false;
    }


    /**
     * {@inheritdoc}
     */
    public function __toString(): string
    {
        return $this->getName();
    }
}
