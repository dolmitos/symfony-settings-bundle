<?php

namespace DolmIT\SettingsBundle\EventSubscriber;

use DolmIT\SettingsBundle\Settings\GlobalSettingsManager;
use DolmIT\SettingsBundle\Settings\SettingsManagerInterface;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class GlobalSettingManagerSubscriber implements EventSubscriberInterface
{
    /**
     * @var SettingsManagerInterface
     */
    private $settingsManager;

    public function __construct(SettingsManagerInterface $settingsManager)
    {
        $this->settingsManager = $settingsManager;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        GlobalSettingsManager::setSettingsManager($this->settingsManager);
    }
    public function onCommandRequest(ConsoleCommandEvent $event)
    {
        GlobalSettingsManager::setSettingsManager($this->settingsManager);
    }

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::REQUEST => array(array('onKernelRequest', 4096)),
            ConsoleEvents::COMMAND => array(array('onCommandRequest', 10)),
        );
    }
}