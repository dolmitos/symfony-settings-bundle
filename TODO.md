
- Create a new setting type that only requires the class to load itself
    - Currently if settings system is used to store only one setting, there is no logical way to obtain the value of the setting 
        - What can be used currently - SettingsManager->loadSettingCollection(className)->first()
            - But that doesn't really make much sense
    - SettingsManager->load(className) - 1 result
    