<?php

namespace DolmIT\SettingsBundle\Normalizer;

use DolmIT\SettingsBundle\Settings\SettingsInterface;
use DolmIT\SettingsBundle\Settings\SettingsManagerInterface;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class SettingsNormalizer implements NormalizerInterface, DenormalizerInterface, CacheableSupportsMethodInterface
{

    /**
     * @var SettingsManagerInterface;
     */
    private $settingsManager;



    /**
     * {@inheritdoc}
     *
     * @throws InvalidArgumentException
     */
    public function normalize($object, $format = null, array $context = array())
    {
        return $object->getValue();
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof SettingsInterface;
    }


    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        return isset(class_implements($type)[SettingsInterface::class]);
    }

    /**
     * {@inheritdoc}
     */
    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function denormalize($data, $class, $format = null, array $context = array())
    {
        return $this->getSettingsManager()->loadSetting($class, $data);
    }



    /**
     * @return SettingsManagerInterface
     */
    public function getSettingsManager(): SettingsManagerInterface
    {
        return $this->settingsManager;
    }

    /**
     * @param SettingsManagerInterface $settingsManager
     *
     * @return $this
     */
    public function setSettingsManager(SettingsManagerInterface $settingsManager): SettingsNormalizer
    {
        $this->settingsManager = $settingsManager;
        return $this;
    }
}
