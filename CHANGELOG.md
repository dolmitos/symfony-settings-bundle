# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.1.1] - 2019-11-01
- Fix issue when translator was not detached from static setting when caching

## [1.1] - 2019-10-02
- Fix critical issue where translator was not set in cases where the setting had just been created
  
  Created a new postCreate event for the setting, which adds the translator. Additionally added logic to 
  postGetDatabase to recalculate the relations. This was already the case, but it should be mentioned
  that a circular reference can occur if a setting references another setting that references itself.
- Improve upon Dynamic setting calls to allow twig conversions to access the requested data
  
  Example: {{ setting.value }} would have given an error earlier, even if value is a valid key.
  Previously you could have only used {{ setting.getValue() }}
  This also allows @method references to work with twig autocomplete in IDEs

## [1.0.9] - 2019-07-22
- Add function to get contant name as the textual value of the setting

## [1.0.8] - 2019-06-20
- Add TagAware cache provider documentation
- Fix issue with setting relation loading from cache

  The unserialized setting did not match the actual setting and had to be loaded from the 
  SettingsManager again. 
  This also creates an issue where circular references would end up in a neverending cycle, 
  as such, it's not possible to use Settings with reference eachother, even through another setting.
  If A cannot be stored before B is complete and B cannot be stored before A is complete, we have an issue.

## [1.0.7] - 2019-01-23
- Fix issue where is and has were not taken into account when gathering SettingsEntity data

## [1.0.6] - 2019-01-09
- Add TagAwareAdapter usage ability
- Add ability to remove cached elements by tag or directly

## [1.0.5] - 2018-10-24
- Add ability to reset single settings cache
- Add self accessor to accessor list when loading variables to setting

## [1.0.4] - 2018-10-17
- Fix issue where settings containing other settings could not be cached

## [1.0.3] - 2018-10-01
- Add Global SettingsManager to console commands

## [1.0.2] - 2018-09-10
- Add denormalize method to SettingsNormalizer
- Allow static settings to be collected
- Fix collection return type

## [1.0.1] - 2018-08-22
- Fix StaticSetting __toString method

## [1.0.0] - 2018-08-17
- Release basic Settings

[Unreleased]: https://gitlab.com/dolmitos/symfony-settings-bundle/compare/v1.1.1...HEAD
[1.1.1]: https://gitlab.com/dolmitos/symfony-settings-bundle/compare/v1.1.1...v1.1
[1.1]: https://gitlab.com/dolmitos/symfony-settings-bundle/compare/v1.1...v1.0.9
[1.0.9]: https://gitlab.com/dolmitos/symfony-settings-bundle/compare/v1.0.9...v1.0.8
[1.0.8]: https://gitlab.com/dolmitos/symfony-settings-bundle/compare/v1.0.8...v1.0.7
[1.0.7]: https://gitlab.com/dolmitos/symfony-settings-bundle/compare/v1.0.7...v1.0.6
[1.0.6]: https://gitlab.com/dolmitos/symfony-settings-bundle/compare/v1.0.6...v1.0.5
[1.0.5]: https://gitlab.com/dolmitos/symfony-settings-bundle/compare/v1.0.5...v1.0.4
[1.0.4]: https://gitlab.com/dolmitos/symfony-settings-bundle/compare/v1.0.4...v1.0.3
[1.0.3]: https://gitlab.com/dolmitos/symfony-settings-bundle/compare/v1.0.3...v1.0.2
[1.0.2]: https://gitlab.com/dolmitos/symfony-settings-bundle/compare/v1.0.2...v1.0.1
[1.0.1]: https://gitlab.com/dolmitos/symfony-settings-bundle/compare/v1.0.0...v1.0.1
[1.0.0]: https://gitlab.com/dolmitos/symfony-settings-bundle/compare/v1.0.0
